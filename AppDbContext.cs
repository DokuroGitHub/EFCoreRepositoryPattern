﻿using Microsoft.EntityFrameworkCore;

public class AppDbContext : DbContext
{
    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        optionsBuilder.UseInMemoryDatabase(databaseName: "MyDB");
    }

    public DbSet<User>? Users { get; set; }
    public DbSet<Post>? Customers { get; set; }
}