public class Post
{
    public int Id { get; set; }
    public int UserID { get; set; }
    public string Content { get; set; }
    //
    public virtual User User { get; set; }
}
