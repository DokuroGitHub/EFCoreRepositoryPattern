public class User
{
    public int Id { get; set; }
    public string Name { get; set; }
    //
    public virtual ICollection<Post> Posts { get; set; }
}
