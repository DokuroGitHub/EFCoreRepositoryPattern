﻿var db = new AppDbContext();

IGenericRepository<User> userRepo = new GenericRepository<User>(db);
IGenericRepository<Post> postRepo = new GenericRepository<Post>(db);

await userRepo.CreateAsync(new User()
{
    Id = 1,
    Name = "User 1"
});
await userRepo.CreateAsync(new User()
{
    Id = 2,
    Name = "User 2"
});

await postRepo.CreateAsync(new Post()
{
    Id = 1,
    UserID = 1,
    Content = "Post 1"
});
await postRepo.CreateAsync(new Post()
{
    Id = 2,
    UserID = 1,
    Content = "Post 2"
});
await postRepo.CreateAsync(new Post()
{
    Id = 3,
    UserID = 2,
    Content = "Post 3"
});

var users = await userRepo.GetAllAsync();
var posts = await postRepo.GetAllAsync();

foreach (var user in users)
{
    Console.WriteLine($"{user.Id} {user.Name}");
    foreach (var post in user.Posts)
    {
        Console.WriteLine($"{post.Id} {post.Content}");
    }
}
Console.WriteLine("-----------------------------------------------");
foreach (var item in posts)
{
    Console.WriteLine($"{item.Id} {item.Content}");
    Console.WriteLine($"{item.User.Id} {item.User.Name}");
}

Console.WriteLine("-----------------------------------------------");

// users = await userRepo.GetAllAsync(includeProperties: "Posts");
// posts = await postRepo.GetAllAsync(includeProperties: "User");

// foreach (var user in users)
// {
//     Console.WriteLine($"{user.Id} {user.Name}");
//     foreach (var post in user.Posts)
//     {
//         Console.WriteLine($"{post.Id} {post.Content}");
//     }
// }
// Console.WriteLine("-----------------------------------------------");
// foreach (var item in posts)
// {
//     Console.WriteLine($"{item.Id} {item.Content}");
//     Console.WriteLine($"{item.User.Id} {item.User.Name}");
// }