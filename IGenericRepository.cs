using System.Linq.Expressions;

interface IGenericRepository<T>
{
    Task<List<T>> GetAllAsync(Expression<Func<T, bool>>? filter = null, string? includeProperties = null);
    Task<T?> GetAsync(Expression<Func<T, bool>>? filter = null, bool tracked = true, string? includeProperties = null);
    Task CreateAsync(T entity);
    //Task UpdateAsync(T entity);
    Task RemoveAsync(T entity);
    Task SaveAsync();
}
